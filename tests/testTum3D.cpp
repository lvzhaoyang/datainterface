#include <src/TUM3D.h>
#include <src/dataReader.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <iostream>

using namespace boost;

int main() {

  //shared_ptr<Tum3D> tum3d(new Tum3D);
  shared_ptr<DataReader> data_reader = make_shared<Tum3D>();

  const size_t source_index = 100;
  cv::Mat depth;
  cv::Mat rgb;

  data_reader->parseSequenceFile(source_index);

  std::cout << "parsing file complete!" << std::endl;


  for(size_t i = 0; i < 5; i++) {
    //data_reader.printSequences(i);

    data_reader->readDepth(source_index, i, depth);
    data_reader->readImage(source_index, i, rgb);

    imshow("depth", depth);
    imshow("rgb", rgb);
    cv::waitKey(0);
  }

  return 0;
}
