/**
 * @file:   Tum3D.h
 * @author: Zhaoyang Lv
 * @brief:  function for using the TUM RGB-D SLAM dataset
 * @date:   Nov.26 2014
 *
 *
 * dataset link:
 *  http://vision.in.tum.de/data/datasets/rgbd-dataset
 */

#pragma once

#include "dataReader.h"

#include <opencv2/opencv.hpp>
#include <fstream>

#include <utility>

#include <gtsam/geometry/Cal3_S2.h>

class Tum3D : public DataReader {

public:
  /// the most intuitive way to make it work
  /// ToDo:
  /// will try to make it more general in the future
  /// will generate a help file to retrieve each dataset
  Tum3D() : DataReader() {
    // freiburg1 dataset
    dataset_idx.insert(std::make_pair<size_t, std::string>(100, "rgbd_dataset_freiburg1_xyz/"));
    dataset_idx.insert(std::make_pair<size_t, std::string>(101, "rgbd_dataset_freiburg1_360/"));

    // freiburg2 dataset
    dataset_idx.insert(std::make_pair<size_t, std::string>(200, "rgbd_dataset_freiburg2_desk/"));
    dataset_idx.insert(std::make_pair<size_t, std::string>(201, "rgbd_dataset_freiburg2_large_with_loop/"));
    dataset_idx.insert(std::make_pair<size_t, std::string>(202, "rgbd_dataset_freiburg2_pioneer_slam/"));
    dataset_idx.insert(std::make_pair<size_t, std::string>(203, "rgbd_dataset_freiburg2_pioneer_slam2/"));
    dataset_idx.insert(std::make_pair<size_t, std::string>(204, "rgbd_dataset_freiburg2_pioneer_slam3/"));

    // freiburg3 dataset
    dataset_idx.insert(std::make_pair<size_t, std::string>(300, "rgbd_dataset_freiburg3_long_office_household/"));
    dataset_idx.insert(std::make_pair<size_t, std::string>(301, "rgbd_dataset_freiburg3_teddy/"));
  }

  ~Tum3D() = default;

  /**
   * Read the camera intrinsics
   * Take the calibrated parameters from
   *  http://vision.in.tum.de/data/datasets/rgbd-dataset/file_formats#intrinsic_camera_calibration_of_the_kinect
   * Only use the calibrated projection matrix
   */
  gtsam::Cal3_S2::shared_ptr cameraIntrinsic(const size_t sindex) const override {

    double fx(525), fy(525), s(0), u0(320), v0(240);
    if(sindex < 200) {          // freiburg1 dataset
      fx = 546.024414;
      fy = 542.211182;
      u0 = 319.711258;
      v0 = 251.374926;
    } else if(sindex < 300) {   // freiburg2 dataset
      fx = 544.074036;
      fy = 542.530457;
      u0 = 324.003673;
      v0 = 247.150259;
    } else {                    // freiburg3 dataset
      fx = 535.433105;
      fy = 539.212524;
      u0 = 320.106653;
      v0 = 247.632132;
    }

    gtsam::Cal3_S2::shared_ptr camera_intr(new gtsam::Cal3_S2(fx, fy, s, u0, v0));
    return camera_intr;
  }

  /**
   * parse the sequence file names
   */
  void parseSequenceFile(const size_t iindex) override;

  /**
   * print the sequence name for a file check
   */
  void printSequences(const size_t iindex);

  /**
   * read the depth image
   */
  void readDepth(const size_t sindex, const size_t iindex, cv::Mat& depthImage) override;

  /**
   * read the RGB image
   */
  void readImage(const size_t sindex, const size_t iindex, cv::Mat& rgbImage) override;

  /**
   * display the depth image
   */
  void displayDepth(const size_t sindex, const size_t iindex, std::string& file) override;

  /**
   * read the RGB image
   */
  void displayImage(const size_t sindex, const size_t iindex, std::string& file) override;

  /**
   * read the accelerometer information
   */
  void readAcceleromter();

  /**
   * read the ground truth data of odometry
   */
  void readGroudTruthData();

private:

};
