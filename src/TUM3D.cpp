/**
 * @file:   Tum3D.cpp
 * @author: Zhaoyang Lv
 * @brief:  function for using the TUM RGB-D SLAM dataset
 * @date:   Nov.26 2014
 *
 *
 * dataset link:
 *  http://vision.in.tum.de/data/datasets/rgbd-dataset
 */

#include <fstream>

#include <boost/lexical_cast.hpp>

#include "TUM3D.h"

using namespace std;
using namespace boost;
using namespace cv;

bool stringNumCompare(const string& left, const string& right) {
  std::string::size_type sz;

  double left_num  = lexical_cast<double>(left);
  double right_num = lexical_cast<double>(right);

  if(left_num < right_num) {
    return true;
  } else {
    return false;
  }
}

void Tum3D::parseSequenceFile(const size_t iindex) {
  string depth_file_dir_list = benchmark_directory + dataset_idx[iindex] + "depth.txt";
  string rgb_file_dir_list = benchmark_directory + dataset_idx[iindex] + "rgb.txt";

  depth_file.open(depth_file_dir_list.c_str());
  rgb_file.open(rgb_file_dir_list.c_str());

  // read the depth file names into the sequence
  if( depth_file.is_open() ) {

    string line;
    while(getline(depth_file, line)) {
      istringstream readin(line);
      string first_str, second_str;
      readin >> first_str;
      if(first_str == "#") {
        continue;
      }
      time_stamp.push_back(first_str);
      depth_time_stamp.push_back(first_str);
      readin >> second_str;
      depth_sequences.push_back(second_str);
    }
  }

  // read the rgb file names into the sequence
  if( rgb_file.is_open() ) {

    string line;
    while(std::getline(rgb_file, line)) {
      istringstream readin(line);
      string first_str, second_str;
      readin >> first_str;
      if(first_str == "#") {
        continue;
      }
      time_stamp.push_back(first_str);
      rgb_time_stamp.push_back(first_str);
      readin >> second_str;
      rgb_sequences.push_back(second_str);
    }

  }

  // sort the timestamp
  sort(time_stamp.begin(), time_stamp.end(), stringNumCompare);

  depth_file.close();
  rgb_file.close();
}

void Tum3D::printSequences(const size_t iindex) {
  cout << "depth is: " << depth_sequences[iindex] << endl;
  cout << "rgb is: " << rgb_sequences[iindex] << endl;
}

void Tum3D::readDepth(const size_t sindex, const size_t iindex, Mat& depthImage) {
  // we didn't readin file name check here, but will add a check for somebody else use.
  string dFile = benchmark_directory + dataset_idx[sindex] + depth_sequences[iindex];
  depthImage = cv::imread(dFile.c_str(), cv::IMREAD_ANYDEPTH);
}

void Tum3D::readImage(const size_t sindex, const size_t iindex, Mat& rgbImage) {
  string rgbFile = benchmark_directory + dataset_idx[sindex] + rgb_sequences[iindex];
  rgbImage = cv::imread(rgbFile.c_str(), cv::IMREAD_ANYCOLOR);
}

void Tum3D::displayDepth(const size_t sindex, const size_t iindex, string& file) {
  Mat depthImage;
  readDepth(sindex, iindex, depthImage);
  imshow(file, depthImage);
}

void Tum3D::displayImage(const size_t sindex, const size_t iindex, string& file) {
  Mat rgbImage;
  readImage(sindex, iindex, rgbImage);
  imshow(file, rgbImage);
}


void Tum3D::readAcceleromter() {
  cout << "Accelerometer reading is empty now." << endl;
}

void Tum3D::readGroudTruthData() {
  cout << "Ground truth reading is empty now." << endl;
}



