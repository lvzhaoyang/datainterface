/**
 * @file:   Tum3D.h
 * @author: Zhaoyang Lv
 * @brief:  general class interface to read different data
 * @date:   Nov.26 2014
 *
 *
 * dataset link:
 *  http://vision.in.tum.de/data/datasets/rgbd-dataset
 */


#pragma once

#include <map>
#include <string>
#include <fstream>

#include <opencv2/opencv.hpp>

#include <boost/lexical_cast.hpp>

#include <gtsam/geometry/Cal3_S2.h>

class DataReader {
public:
  /**
   * constructor
   */
  DataReader()
    : benchmark_directory("/home/zhaoyang/Develop/data/") {
  }

  ~DataReader() = default;

  /**
   * parse the sequence file names
   */
  virtual void parseSequenceFile(const size_t iindex) = 0;
  /**
   * read the depth image
   */
  virtual void readDepth(const size_t sindex, const size_t iindex, cv::Mat& depthImage) = 0;
  /**
   * read the RGB image
   */
  virtual void readImage(const size_t sindex, const size_t iindex, cv::Mat& rgbImage) = 0;
  /**
   * read the depth image
   */
  virtual void displayDepth(const size_t sindex, const size_t iindex, std::string& file) = 0;
  /**
   * read the RGB image
   */
  virtual void displayImage(const size_t sindex, const size_t iindex, std::string& file) = 0;

  /**
   * get methods
   */
  virtual gtsam::Cal3_S2::shared_ptr cameraIntrinsic(const size_t sindex) const = 0;

  const size_t numDepthImages() const {
    return depth_sequences.size();
  }

  const size_t numRgbImages() const {
    return rgb_sequences.size();
  }

  const double depthTimeStamp(const size_t iindex) const {
    return boost::lexical_cast<double>(depth_time_stamp[iindex]);
  }

  const double rgbTimeStamp(const size_t iindex) const {
    return boost::lexical_cast<double>(rgb_time_stamp[iindex]);
  }

protected:
  /** the home directory for the benchmark data */
  std::string benchmark_directory;
  /** dataset idx for the file names */
  std::map<size_t, std::string> dataset_idx;

  /** the file that stores the depth image names */
  std::ifstream depth_file;
  /** the file that stores the rgb image names */
  std::ifstream rgb_file;

  /** the depth file sequences */
  std::vector<std::string> depth_sequences;
  /** the rgb file sequences */
  std::vector<std::string> rgb_sequences;

  /** the time stamp vector*/
  std::vector<std::string> time_stamp;
  std::vector<std::string> rgb_time_stamp;
  std::vector<std::string> depth_time_stamp;

};
