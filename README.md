# README Data Interface#

### What is this Data Interface repository for? ###
This library is for a general benchmark dataset reading library, still in construction. I will use it to handle all the different benchmark data.

### QuickStart ###

* Configuration
In the root directory folder:

```
#!Shell

$ mkdir build
$ cd build
$ cmake ..
$ make install
```
The benchmark data directory is placed at my machine at /home/zhaoyang/Develop/data, replace the line in src/dataReader.h file.
Download the relevant dataset and put the in the data folder.

I will fix this hard code soon.


* Dependencies
Need OpenCV and gtsam

For gtsam, download the most recent release version from [](https://collab.cc.gatech.edu/borg/gtsam?destination=node%2F299)

* How to run tests
All the tests are in the tests folder
